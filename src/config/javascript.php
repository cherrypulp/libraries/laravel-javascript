<?php

return [
    /**
     * @var {string} Default namespace (default: "__app")
     */
    'namespace' => "__app"
];
